# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     Project -> flask_web.py
   Description :   web应用程序接受接口
   Author :        William
   date：          2020/2/21/0021
-------------------------------------------------
   Change Activity:
                   2020/2/21/0021: 文件初创
                   2020/2/27：   增加时间管理功能

-------------------------------------------------
"""

import base64,json,datetime
from flask import Flask, request, jsonify
from urllib.parse import unquote
from Config.Setting import *


def bs64_url_data_dncode(con):
    '''进行解码 先url解码 -> 然后 bs64解码'''
    return unquote(base64.b64decode(con).decode('utf-8'))

app = Flask(__name__)

@app.route("/send/", methods=["POST", "GET"])
def index():
    if request.method == "POST":
        kam = bs64_url_data_dncode(request.form.get("kam")) #消息类型
        if kam not in MESSAGE_TYPE:
            return jsonify({"message":"不在规定的消息类型中"})
        #一下是公共参数
        from_wxid = bs64_url_data_dncode(request.form.get("from_wxid"))  # 消息一级来源微信id，如果是群聊的话，是群id
        from_name = bs64_url_data_dncode(request.form.get("from_name"))  # 消息一级来源的昵称
        msg = bs64_url_data_dncode(request.form.get("msg")).replace(r"[@emoji=\u00A0]", " ").replace("\n"," ") #消息
        robot_wxid = bs64_url_data_dncode(request.form.get("robot_wxid"))  # 机器人微信id
        final_from_wxid = bs64_url_data_dncode(request.form.get("final_from_wxid",""))  # 消息二级来源
        final_nickname = bs64_url_data_dncode(request.form.get("final_from_name",""))  # 消息二级来源昵称
        msg_dict = {
            "kam":kam,
            "from_wxid":from_wxid,
            "from_name":from_name,
            "msg":str(msg).replace("\r","").replace("\n","").replace("\t",""),
            "robot_wxid":robot_wxid,
            "final_from_wxid":final_from_wxid,
            "final_nickname":final_nickname,
        }
        if kam == "qlxx":
            message_queue.put(msg_dict)
            logger.info(f"数据存储成功，消息类型：{kam},消息一级来源：{from_name}")
        elif kam == "slxx":
            logger.info("私聊消息！")
        return "post请求"
    elif request.method == "GET":
        logger.info("GET请求测试")
        return "GET测试页面"

