# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     setting.py
   Description :   配置文件
   Author :        William
   date：          2020/2/20
-------------------------------------------------
   Change Activity:
                   2020/2/20: 文件初创
                   2020/2/27：增加时间管理配置

-------------------------------------------------
"""

import os,traceback
from loguru import logger
from concurrent.futures import ThreadPoolExecutor
from queue import Queue

#消息存储队列
message_queue = Queue(maxsize=0)

#线程数量，用来做数据同步，推送使用
executor = ThreadPoolExecutor(4)
#根目录
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
#日志配置项
LOG_DIR = os.path.join(BASE_DIR, "log")
if not os.path.exists(LOG_DIR):
    try:
        os.makedirs(LOG_DIR)
    except:
        traceback.print_exc()
logger.add(os.path.join(LOG_DIR,"{time:YYYY-MM-DD}.log"),
           format="{time:YYYY-MM-DD at HH:mm:ss} - {level} - {message}",
           rotation="00:00",
           enqueue=True)

#可爱猫相关配置
Send_Url = {
    "wxid_7jyd7rpc****": "http://192.168.66.30:2020/Send/", #测试号1
    "wxid_pac50w23****": "http://192.168.66.103:2020/Send/",#测试号2
}

#Web Server 相关配置项
SERVER_API = {
    "HOST": "0.0.0.0",  # The ip specified which starting the web API
    "PORT": 8076,  # port number to which the server listens to
    "DEBUG":False
}