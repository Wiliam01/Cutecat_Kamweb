
import time
from Config.Setting import *

def message_read():
    logger.info("消息处理任务开始！")
    while True:
        try:
            if not message_queue.empty():
                logger.debug(f"开始读取消息队列,目前队列中的消息数量为：{message_queue.qsize()}")
                message_data = message_queue.get()
                print(message_data)
            else:
                logger.info("当前消息队列为空")
        except:
            traceback.print_exc()
        time.sleep(60*1)