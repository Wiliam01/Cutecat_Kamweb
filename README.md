# Cutecat_Kamweb

#### 介绍
可爱猫Kamweb插件 开发demo Python 版本

#### 软件架构
可爱猫 - http://www.keaimao.com/ （Windows端协议）    
插件 - Kamweb    
文档地址：https://mubu.com/doc/6DfKlgxdsnP


#### 安装教程
1.安装第三方库
```shell script
pip install requests flask loguru -i https://pypi.douban.com/simple
```
2.配置Setting文件
```shell script
目录位置 ： Config -> Setting

1）配置可爱猫推送接口
Send_Url = {
    "wxid_7jyd7rpc****": "http://192.168.66.30:2020/Send/", #测试号1
    "wxid_pac50w23****": "http://192.168.66.103:2020/Send/",#测试号2
}
    配置两个号的原因就是，有可能是多个微信，但是怕封号，采取以下措施：
    1.1分布在多台电脑或者是虚拟机上，所以会采取一对多的方式
    1.2同时增加队列，如果你不频繁或者同时发消息给多个好友和群其实也是没事的
2）配置Flask web应用
SERVER_API = {
    "HOST": "0.0.0.0",  # The ip specified which starting the web API
    "PORT": 8076,  # port number to which the server listens to
    "DEBUG":False
}

3）其他配置项根据自己的需求调整即可

```

#### 使用说明

直接运行 根目录的 Run文件即可

#### 参与贡献

[William](https://gitee.com/Wiliam01)

#### 帮助与支持

QQ:3318223374   
如果有问题，同时你也可以提交[issue](https://gitee.com/Wiliam01/Cutecat_Kamweb/issues)
